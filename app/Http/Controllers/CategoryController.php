<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
     return view('admin.category.add-category');
    }

    public function categorySave(Request $request){
     $category = new Categories();
     $category -> category_name         = $request -> category_name;
     $category -> category_description  = $request -> category_description;
     $category -> publication_status    = $request -> publication_status;
     $category -> save();
     return redirect('/add-category')->with('message','Category save successfully');
    }
    public function manageCategory(){
        $categories = Categories::all();
        return view('admin.category.manage-category',['categories' =>$categories]);
    }
    public function unpublishedCategory($id){
      $category = Categories::find($id);
      $category -> publication_status = 0;
      $category -> save();
      return redirect('/manage/category')->with('message','Category unpublished successfully');

    }
    public function publishedCategory($id){
        $category = Categories::find($id);
        $category -> publication_status = 1;
        $category -> save();
        return redirect('/manage/category')->with('message','Category published successfully');
    }
    public function editCategory($id){
        $category = Categories::find($id);
        return view('admin.category.edit',['category' =>$category]);
    }
    public function updateCategory(Request $request){
        $category = Categories::find($request->category_id);
        $category -> category_name         = $request -> category_name;
        $category -> category_description  = $request -> category_description;
        $category -> publication_status    = $request -> publication_status;
        $category -> save();
        return redirect('/manage/category')->with('message','Information Update Successfully');
    }
    public function deleteCategory($id){
        $category = Categories::find($id);
        $category->delete();
        return redirect('/manage/category')->with('message','Delete Category');
    }
}
