<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(){
        return view('admin.brand.add-brand');
    }
    public function brandSave(Request $request){
        $this->validate($request,[
           'brand_name'          =>'required',
           'brand_description'   =>'required',
           'publication_status'  =>'required',
        ]);
     $brand = new Brand();
     $brand -> brand_name            = $request -> brand_name;
     $brand -> brand_description     = $request -> brand_description;
     $brand -> publication_status    = $request -> publication_status;
     $brand ->save();
     return redirect('/add/brand')->with('message','Add Brand Successfully');
    }
    public function brandManage(){
        $brands = Brand::all();
        return view('admin.brand.manage-brand',['brands' => $brands]);
    }
    public function brandUnpublish($id){
     $brand = Brand::find($id);
     $brand ->publication_status = 0;
     $brand ->save();
     return redirect('/brand/manage')->with('message','Publication unpublished');
    }
    public function brandPublish($id){
        $brand = Brand::find($id);
        $brand ->publication_status = 1;
        $brand ->save();
        return redirect('/brand/manage')->with('message','Publication published');
    }
    public function brandEdit($id){
     $brand = Brand::find($id);
     return view('admin.brand.edit-brand',['brand'=>$brand]);
    }
    public function brandUpdate(Request $request){
        $brand = Brand::find($request->brand_id);
        $brand -> brand_name            = $request -> brand_name;
        $brand -> brand_description     = $request -> brand_description;
        $brand -> publication_status    = $request -> publication_status;
        $brand ->save();
        return redirect('/brand/manage')->with('message','Edit Brand Successfully');
    }
    public function brandDelete($id){
      $brand = Brand::find($id);
      $brand->delete();
      return redirect('/brand/manage')->with('message','Delete Brand Successfully');
    }
}
