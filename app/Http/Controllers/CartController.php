<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Cart;
class CartController extends Controller
{
    public function addtoCart(Request $request){
    //   return $request->all();
      $product = Product::find($request->id);


Cart::add([
    'id' => $request->id,
     'name' => $request->product_name,
      'price' =>$request->product_price,
      'qty' =>$request->qty,
    ]);
   return "ok";
    }
}
