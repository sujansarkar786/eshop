<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Categories;
use App\Product;
use Illuminate\Http\Request;
use DB;

class productController extends Controller
{
    public function index(){

        $categories  = Categories::where('publication_status', 1)->get();
        $brands     = Brand::where('publication_status', 1)->get();

        return view('admin.product.add-product',[
            'brands'     => $brands,
            'categories'  => $categories,
        ]);
    }
    protected function validateProduct($request){
        $this->validate($request,[
            'category_id' => 'required',
            'brand_id' => 'required',
            'product_name' => 'required',
            'product_price' => 'required',
            'product_quantity' => 'required',
            'product_short_description' => 'required',
            'product_long_description' => 'required',
            'product_image' => 'required',
            'publication_status' => 'required',

        ]);
    }
    protected function imageUpload($request){
        $file_image         =   $request ->file('product_image');
        $imageName          =   $file_image->getClientOriginalName();
        $directorytImage    =   'productDirectory/';
        $imageUrl           =   $directorytImage.$imageName;
        $file_image         ->   move($directorytImage,$imageName );
        return  $imageUrl;
    }
    protected function saveProductInfo($request, $imageUrl){
        $product = new Product();
        $product    ->category_id                = $request ->category_id;
        $product    ->brand_id                   = $request ->brand_id;
        $product    ->product_name               = $request ->product_name;
        $product    ->product_price              = $request ->product_price;
        $product    ->product_quantity           = $request ->product_quantity;
        $product    ->product_short_description  = $request ->product_short_description;
        $product    ->product_long_description   = $request ->product_long_description;
        $product    ->product_image              = $imageUrl;
        $product    ->publication_status         = $request ->publication_status;
        $product    ->save();
    }
    public function saveProduct(Request $request){
        $this->validateProduct($request);
        $imageUrl  =  $this->imageUpload($request);
        $this->saveProductInfo($request, $imageUrl);
        return redirect('/add/product')->with('message','Product save successfully');

    }
    public function manageProduct(){
        $products =  DB::table('products')
                 ->join('categories','products.category_id', '=','categories.id')
                 ->join('brands','products.brand_id', '=', 'brands.id')
                 ->select('products.*','categories.category_name', 'brands.brand_name')
                 ->get();

        // return $products;
        return view('admin.product.manage-product',['products' => $products]);
    }

    public function publicProduct($id){
      $product = Product::find($id);
      $product ->publication_status = 0;
      $product ->save();
      return redirect('/manage/product')->with('message','Public this items');
    }

    public function unpublicProduct($id){
      $product = Product::find($id);
      $product  ->publication_status = 1;
      $product  ->save();
      return redirect('/manage/product')->with('message','Public this items');
    }
    public function editProduct($id){
    $products = Product::find($id);
    $categories  = Categories::where('publication_status', 1)->get();
    $brands     = Brand::where('publication_status', 1)->get();

    return view('admin.product.edit-product',[
         'products' => $products,
         'categories' => $categories,
         'brands'    => $brands
         ]);
    }
    public function updateProductInfo($request,$product,$imageUrl = null){
        $product    ->category_id                = $request ->category_id;
        $product    ->brand_id                   = $request ->brand_id;
        $product    ->product_name               = $request ->product_name;
        $product    ->product_price              = $request ->product_price;
        $product    ->product_quantity           = $request ->product_quantity;
        $product    ->product_short_description  = $request ->product_short_description;
        if($imageUrl){
        $product    ->product_image              = $imageUrl;
        }
        $product    ->product_long_description   = $request ->product_long_description;
        $product    ->publication_status         = $request ->publication_status;
        $product    ->save();
    }
    public function updateProduct(Request $request){
   $productImage = $request->file('product_image');
   $product =  Product::find($request->product_id);

        if($productImage){
            $product =  Product::find($request->product_id);
            unlink($product->product_image);
            $imageUrl= $this->imageUpload($request);
            $this-> updateProductInfo($request,$product,$imageUrl);

        }else{
            $this-> updateProductInfo($request,$product);
        }
      return  redirect('/manage/product')->with('message','Product is uploaded successfully');
    }
    public function deleteProduct(){

    }
}
