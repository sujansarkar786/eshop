<?php

namespace App\Http\Controllers;
use App\Categories;
use App\Product;
use Illuminate\Http\Request;

class NewShopController extends Controller
{
    public function index(){
        $newProducts = Product::where('publication_status',1)
                    ->orderBy('id','DESC')
                    ->take(8)
                    ->get();

        return view('front-end.home.home',[
            'newProducts' => $newProducts
        ]);
    }
    // Front End Part
    public function categoryProduct($id){
        $categoryproducts = Product::where('category_id', $id)
                    ->where('publication_status', 1)
                    ->get();

        return view('front-end.category.category',['categoryproducts'=>$categoryproducts]);
    }
    public function categorySingle($id){
        $product = Product::find($id);
        return view('front-end.category.single-product',['product' => $product]);
    }
}
