<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'NewShopController@index')->name('/');
Route::get('/category-product/{id}', 'NewShopController@categoryProduct')->name('category-product');
// Single Product
Route::get('/single/product/{id}/{name}','NewShopController@categorySingle')->name('single-product');
Route::get('/single/product/{id}','NewShopController@categorySingle')->name('single-product');

// Cart controller
Route::post('/addTo/cart','CartController@addtoCart')->name('add-to-cart');

// Back end
Route::get('/add-category', 'CategoryController@index')->name('add-category');
Route::get('/manage/category', 'CategoryController@manageCategory')->name('manage-category');
Route::get('/unpublished/category/{id}', 'CategoryController@unpublishedCategory')->name('unpublished-category');
Route::get('/published/category/{id}', 'CategoryController@publishedCategory')->name('published-category');
Route::get('/edit/category/{id}', 'CategoryController@editCategory')->name('edit-category');
Route::get('/delete/category/{id}', 'CategoryController@deleteCategory')->name('delete-category');
Route::post('/update/category', 'CategoryController@updateCategory')->name('update-category');
Route::post('/category/save', 'CategoryController@categorySave')->name('new-category');

// Brannd Product
Route::get('/add/brand','BrandController@index')->name('add-brand');
Route::post('/brand/save','BrandController@brandSave')->name('new-brand');
Route::get('/brand/manage','BrandController@brandManage')->name('manage-brand');
Route::get('/brand/unpublish/{id}','BrandController@brandUnpublish')->name('unpublish-brand');
Route::get('/brand/publish/{id}','BrandController@brandPublish')->name('publish-brand');
Route::get('/brand/edit/{id}','BrandController@brandEdit')->name('edit-brand');
Route::post('/brand/update','BrandController@brandUpdate')->name('update-brand');
Route::get('/brand/delete/{id}','BrandController@brandDelete')->name('delete-brand');

// Product Section
Route::get('/add/product','productController@index')->name('product-add');
Route::get('/manage/product','productController@manageProduct')->name('manage-product');
Route::post('/save/product','productController@saveProduct')->name('add-product');
Route::post('/update/product','productController@updateProduct')->name('update-product');
Route::get('/public/product/{id}','productController@publicProduct')->name('unpublished-product');
Route::get('/unpublic/product/{id}','productController@unpublicProduct')->name('published-product');
Route::get('/edit/product/{id}','productController@editProduct')->name('edit-product');
Route::get('/delete/product/{id}','productController@deleteProduct')->name('delete-product');



// Autitication Model
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
