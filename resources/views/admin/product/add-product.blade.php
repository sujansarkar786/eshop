@extends('admin.master')
@section('body')
<div class="container p-3">
    <div class="row p-3 mb-2 bg-success text-white">
        <div class="col-md-10 p-5">
            <div class="p-3 mb-2 text-white text-center">
                <h3 class="text-canter">Title panel</h3>
                {{ Session::get('message') }}
            </div>
        <form action="{{route('add-product')}}" method="POST" enctype="multipart/form-data">
            @csrf

                <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Category Name</label>
                    <div class="col-sm-9">
                     <select name="category_id" id="" class="form-control">
                         <option value="">===Select Option===</option>
                         @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                         @endforeach
                     </select>
                    </div>
                </div>
                <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Brand Name</label>
                    <div class="col-sm-9">
                     <select name="brand_id" id="" class="form-control">
                         <option value="">===Select Option===</option>
                         @foreach ($brands as $brand)
                         <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                      @endforeach
                     </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Name:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control form-control-lg" id="colFormLabelLg" name="product_name" placeholder="Enter Product Name">
                    <span>{{ $errors ->has('product_name') ? $errors->first('product_name'): ''}}</span>
                  </div>
                 </div>
                 <div class="form-group row">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Price:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control form-control-lg" id="colFormLabelLg" name="product_price" placeholder="Enter Product Price">
                    <span>{{ $errors ->has('product_price') ? $errors->first('product_price'): ''}}</span>
                  </div>
                 </div>
                 <div class="form-group row">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Quantity:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control form-control-lg" id="colFormLabelLg" name="product_quantity" placeholder="Enter Product quantity">
                    <span>{{ $errors ->has('product_quantity') ? $errors->first('product_quantity'): ''}}</span>
                  </div>
                 </div>
                 <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Short Description</label>
                    <div class="col-sm-9">
                      <textarea name="product_short_description"  class="form-control form-control-lg fr-view"  cols="10" rows="10"></textarea>
                      <span>{{ $errors ->has('product_short_description') ? $errors->first('product_short_description'): ''}}</span>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Long Description</label>
                    <div class="col-sm-9">
                      <textarea name="product_long_description"  class="form-control form-control-lg fr-view"  cols="10" rows="10"></textarea>
                      <span>{{ $errors ->has('product_long_description') ? $errors->first('product_long_description'): ''}}</span>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Product Image Upload</label>
                    <div class="col-sm-9">
                      <input type="file" name="product_image"  class="form-control form-control-lg">
                      <span>{{ $errors ->has('product_image') ? $errors->first('product_image'): ''}}</span>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Publication Status:</label>
                    <div class="col-sm-9">
                    <label for=""><input type="radio" name="publication_status" value="1">publish</label>
                    <label for=""><input type="radio" name="publication_status" value="0">Unpublish</label>
                    <span>{{ $errors ->has('publication_status') ? $errors->first('publication_status'): ''}}</span>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Save information</label>
                    <div class="col-sm-9">
                        <input role="button" type="submit" name="btn" class="btn btn-secondary" value="Save value">
                    </div>
                  </div>
              </form>
        </div>
    </div>
@endsection
