@extends('admin.master')
@section('body')
<div class="container p-3">
    <div class="row p-3 mb-2">
    <h3>{{ Session::get('message')}}</h3>
        <div class="col-md-10 p-5">
        <table class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Sl No</th>
                <th scope="col">Category Name</th>
                <th scope="col">Categpry Description</th>
                <th scope="col">Publication Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @php ($i=1);
                @foreach ($categories as $category)
                <tr>
                <th scope="row">{{ $i++ }}</th>
                    <td>{{ $category->category_name }}</td>
                    <td>{{ $category->category_description }}</td>
                    <td>{{ $category->publication_status == 1 ? 'published': 'unpublished' }}</td>
                    <td>
                        @if($category->publication_status == 1 )
                    <a href="{{route('unpublished-category', ['id' => $category -> id])}}">
                          <span><i class="fas fa-arrow-up"></i></span>
                      </a>
                      @else
                      <a href="{{route('published-category', ['id' => $category -> id])}}">
                        <span><i class="fas fa-arrow-down"></i></span>
                    </a>
                    @endif
                    <a href="{{route('edit-category', ['id' => $category -> id])}}">
                        <span><i class="far fa-edit"></i></span>
                    </a>
                    <a href="{{route('delete-category', ['id' => $category -> id])}}">
                        <span><i class="far fa-trash-alt"></i></span>
                    </a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </div>
@endsection


