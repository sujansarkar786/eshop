@extends('admin.master')
@section('body')
<div class="container p-3">
    <div class="row p-3 mb-2 bg-success text-white">
        <div class="col-md-10 p-5">
            <div class="p-3 mb-2 text-white text-center">
                <h3 class="text-canter">Title panel</h3>
                {{ Session::get('message') }}
            </div>
        <form action="{{route('new-category')}}" method="POST">
            @csrf
                <div class="form-group row">
                  <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Category Name:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control form-control-lg" id="colFormLabelLg" name="category_name" placeholder="Enter Category name">
                  </div>
                </div>
                <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Category Description:</label>
                    <div class="col-sm-9" id="editor">
                      <textarea name="category_description"  class="form-control form-control-lg fr-view"  cols="30" rows="10"></textarea>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Publication Status:</label>
                    <div class="col-sm-9">
                    <label for=""><input type="radio" name="publication_status" value="1">publish</label>
                    <label for=""><input type="radio" name="publication_status" value="0">Unpublish</label>
                    </div>
                  </div>
                  <div class="form-group row ">
                    <label for="colFormLabelLg" class="col-sm-3 col-form-label col-form-label-lg">Save information</label>
                    <div class="col-sm-9">
                        <input role="button" type="submit" name="btn" class="btn btn-secondary" value="Save value">
                    </div>
                  </div>
              </form>
        </div>
    </div>
@endsection
