@extends('admin.master')
@section('body')
<div class="container p-3">
    <div class="row p-3 mb-2">
    <h3></h3>
        <div class="col-md-10 p-5">
        <table class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Sl No</th>
                <th scope="col">Category Name</th>
                <th scope="col">Categpry Description</th>
                <th scope="col">Publication Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @php($i = 1)
                @foreach ($brands as $brand)
                <tr>
                    <th scope="row">{{ $i++ }}</th>
                        <td>{{ $brand->brand_name }}</td>
                        <td>{{ $brand->brand_description }}</td>
                        <td>{{ $brand->publication_status ==1 ? 'publishd': 'unpublished' }}</td>
                        <td>
                        @if($brand->publication_status ==1)
                        <a href="{{ route('unpublish-brand',['id' =>$brand->id]) }}">
                              <span><i class="fas fa-arrow-up"></i></span>
                         </a>
                        @else
                          <a href="{{ route('publish-brand',['id' =>$brand->id]) }}">
                            <span><i class="fas fa-arrow-down"></i></span>
                        </a>
                        @endif
                        <a href="{{ route('edit-brand',['id' =>$brand->id]) }}">
                            <span><i class="far fa-edit"></i></span>
                        </a>
                        <a href="{{ route('delete-brand',['id' =>$brand->id]) }}">
                            <span><i class="far fa-trash-alt"></i></span>
                        </a>
                        </td>
                      </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </div>
@endsection
